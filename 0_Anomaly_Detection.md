# Anomaly Detection

>Hold what you really know and tell what you do not know -this will lead to knowledge.
>--Confucious

>Machine knows I don't know
Machine knows I know

## Problem Formulation

- Given a set of training data ***x***  = $`\{x^1, x^2, ... x^N\}`$
- Anomaly Detector (AD) :
	- **x** similar to training data => AD => Normal
	- **x** different from training data => AD => Anomaly (outlier, novelty, exceptions)
	```mermaid
	graph TD;
	A[x similar to training data ] -->B[Anomaly Detector];
	B-->C[Normal];
	D[x different from training data]-->E[Anomaly Detector];
	E-->G[Anomaly];

	```
- Different approaches use different ways to determine the similarity

- Applications
	- Fraud Detection
	- Network Intrusion Detection
	- Cancer Detection



## Can't Be Binary Classification 

- **It is difficult to find anomaly example**

- Given normal data $`\{x^1, x^1, ... x^N\}`$ => Class 1
- Given anomaly data $`\{\tilde x^1, \tilde x^2, ... \tilde x^N\}`$  => Class 2
- Then training binary classifier...
- NOT **x** cannot considered as a class

---

## Catagories

```mermaid

graph LR;

A[Training data x] -->B[With labels];
B-->C[Classifier];
A--> D[Unlabeled];
D-->E[Clean: All the training data is normal];
D-->F[Polluted : A little bit of training data is anomaly];

```

- Training data $`\{x^1, x^2, ... x^N\}`$
- With labels $`\{\hat {y}^1, \hat {y}^2, ..., \hat {y}^N \}`$
- The classifier can output "unknown" (none of the training data is labelled "unknown") -> Open-set Recognition

---

### Case 1 : With Classifier

How to use the Classifier

```mermaid
graph LR;
A[Input x]-->B["Classifier (with Softmax layer)"];
B-->C[Class y ];
B-->D[Confidence score c]
```

- Anomaly Detection :

	$`f(x) = \begin{cases}normal & c(x)> \lambda \\anomaly&c(x)\leq \lambda \end{cases}`$

- Estimate Confidence => Output of softmax 
	- The maximum scores
	- Negative entropy


### Framework -> Example: Simpsons

- Training Set : Images x of characters from Simpsons. Each image x is labelled by its characters $`\hat y`$.
	- Train a classifier, and we can obtain confidence score c(x) from the classifier.
	- $`f(x) = \begin{cases}normal & c(x)> \lambda \\anomaly&c(x)\leq \lambda \end{cases}`$
- Dev Set : Images x : **Label each image x is from Simpsons or not**
	- We can compute the **_performance_** of $`f(x)`$ using dev set to determine $`\lambda`$ and other hyperparameters.

- Testing set :Images x -> from Simpsons or not



### Evaluation
- Accuracy is not a good measurement
	- A system can have high accuracy, but do nothing => due to small amount of anomal data
- Use Detected Table and evaluate cost (depend on false alarm/missing case) 
-  EX :$`\lambda_1, \lambda_2`$ Detected Table

	|$`\lambda_1`$|Anomaly|Normal|
	|---|---|---|
	|Detected| 1|1 (False alarm)|
	|Not Detected|4 (Missing)|99|

	|$`\lambda_2`$|Anomaly|Normal|
	|---|---|---|
	|Detected|2|6 (False alarm)|
	|Not Detected|3 (Missing)|94|

- Cost Table : Different cost table for different task
	- Cost Table A : $`Cost_{\lambda_1} = 104`$,  $`Cost_{\lambda_2} = 603`$
 	
		|Cost Table A|Anomaly|Normal|
		|---|---|---|
		|Detected| 0|100 (False alarm)|
		|Not Detected|1 (Missing)|0|
	- Cost Table B: $`Cost_{\lambda_1} = 401`$,  $`Cost_{\lambda_2} = 306`$

		|Cost Table B|Anomaly|Normal|
		|---|---|---|
		|Detected| 0|1 (False alarm)|
		|Not Detected|100 (Missing)|0|


- Other method:
	- Area under ROC curve (condider the ranking)



##### Learn more

- Learn a classifier giving low confidence score to anomaly
- GAN with some constraints to obtain anomaly
- [Training Confidence-calibrated Classifiers for Detecting Out-of-Distribution Samples](https://arxiv.org/abs/1711.09325)



---

### Case 2 : Without labels

-Example : Using anomaly detection, can we identify the "Troll" ?

- Paper : [Twitch Plays Pokemon, Machine Learns Twitch: Unsupervised Context-Aware Anomaly Detection for Identifying Trolls in Streaming Data](https://arxiv.org/abs/1902.06208)
- Database: [twitch-troll-detection](https://github.com/ahaque/twitch-troll-detection)

- Generate a propbability model $`P(x)`$ =>Maximum Likelihood
	-  Assuming the data points is sampled from a probability density function $`f_{\theta}(x)`$
	- $`\theta`$ determines the shape of $`f_{\theta}(x)`$
	- $`\theta`$ is unknown, to be found from data
	
			
		|$`L(\theta) = f_{\theta}(x^1)f_{\theta}(x^2)...f_{\theta}(x^N)`$|
		|---|
		|$`\theta^* = arg max_{\theta}L(\theta)`$|


	- Example with Gaussian Distribution => Strong assumption

- outlook :
	-  Auto-encoder
	-  One-class SVM
	-  Isolated Forest
---

## Reference

- [Learning Confidence for Out-of-Distribution Detection in Neural Networks](https://arxiv.org/abs/1802.04865)
- [Training Confidence-calibrated Classifiers for Detecting Out-of-Distribution Samples](https://arxiv.org/abs/1711.09325)
- [Twitch Plays Pokemon, Machine Learns Twitch: Unsupervised Context-Aware Anomaly Detection for Identifying Trolls in Streaming Data](https://arxiv.org/abs/1902.06208)