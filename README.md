# The Next Step of Machine Learning
EN learning notes of [The Next Step of Machine Learning ](https://www.youtube.com/playlist?list=PLJV_el3uVTsOK_ZK5L0Iv_EQoL1JefRL4) courses taught by [Hung-yi Lee](https://www.youtube.com/channel/UC2ggjtuuWvxrHHHiaDH1dlQ)


- [Anomaly Detection](0_Anomaly_Detection.md)
- Explainable AI
- Preventing Adverarial Attack
- Life-long Learning
- Meta-learning / Learn to learn
- Few-shot learning / Zero-shot learning
- Reinforcement learning?
- Network Compression
- Difference between Training Data and Testing Data



### Referernce
[The Next Step of Machine Learning ](https://www.youtube.com/playlist?list=PLJV_el3uVTsOK_ZK5L0Iv_EQoL1JefRL4)